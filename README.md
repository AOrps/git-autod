# git-autod
- daemon to auto-commit (and push) git commits from LLM generated comments


## :mag: Manual

```sh

```

## :hammer: How to build


## :alembic: Usage


---

## :framed_picture: Screenshots / Images


## :card_file_box: Directory Explanation
<!-- tree command: tree -d --noreport -->

```s


```


## :warning: Disclaimer
- You have the potential to auto-commit potentially sensitive things
- Thereotically (when the hash of string feature is complete), this is implicitly vulnerable to a SHA collision attack. (But given that it's using SHA-512 (or it will), have fun with that) 
