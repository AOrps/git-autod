package main

import (
	"context"
	"log"
	"os"

	. "gitlab.com/AOrps/git-autod/cli"
)

const (
	LLM_PATH = "/etc/git-autod/model"
	LOCAL_LLM_PATH = "./model"
	LLM_MODEL = "gg"
	MODEL_URL = "https://huggingface.co/TheBloke/Guanaco-7B-Uncensored-GGUF/resolve/main/guanaco-7b-uncensored.Q3_K_M.gguf?download=true"
	TEST_MODEL_URL = "https://huggingface.co/tsunemoto/TinyDolphin-2.8-1.1b-GGUF/resolve/main/tinydolphin-2.8-1.1b.Q3_K_S.gguf?download=true"
	MODEL_NAME = "guanaco-7b-uncensored.gguf"
	TEST_MODEL_NAME = "tinydolphin-2.8-1.1b.gguf"
)

func main() {

	ctx := context.Background()
	// ctx = context.WithValue(ctx, "LLM_PATH", LLM_PATH)
	// ctx = context.WithValue(ctx, "MODEL_URL", MODEL_URL)
	// ctx = context.WithValue(ctx, "MODEL_NAME", MODEL_NAME)
	
	ctx = context.WithValue(ctx, "LOCAL_LLM_PATH", LOCAL_LLM_PATH)
	ctx = context.WithValue(ctx, "TEST_MODEL_URL", TEST_MODEL_URL)
	ctx = context.WithValue(ctx, "TEST_MODEL_NAME", TEST_MODEL_NAME)

	cmd := CMD()

	if err := cmd.Run(ctx, os.Args); err != nil {
		log.Fatal(err)
	}
}
