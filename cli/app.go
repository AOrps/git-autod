package cli

import (
	"context"
	// "flag"
	"fmt"

	"github.com/urfave/cli/v3"
)

// const (
// 	BASE_USER = "TheBloke"
// 	BASE_MODEL = "Guanaco-7B-Uncensored-GGUF"
// 	// MODEL
// )

func commands() []*cli.Command {

	return []*cli.Command{
		{
			Name: "init",
			Usage: "Initialize ",
			Action: func (ctx context.Context, cmd *cli.Command) error {
				return Init(ctx, cmd)
			},
		},
		{
			Name: "run",
			Usage: "Run at specific directory",
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name: "directory",
					Value: "~/projects",
					Usage: "directory for investigate",
				},
			},
			Action: func (ctx context.Context, cmd *cli.Command) error {
				// fmt.Println(cmd.Flags)
				fmt.Println(cmd.String("directory"))
				// fmt.Println(cmd.StringFlag("directory"))
				return Run(ctx, cmd)
			},
		},
	}
	
	
}

func CMD() *cli.Command {

	cmd := &cli.Command{
		EnableShellCompletion: true,
		Commands: commands(),
	}
	
	return cmd
	
}

