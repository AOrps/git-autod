package cli

import (
	"context"
	"fmt"

	"github.com/urfave/cli/v3"
	"gitlab.com/AOrps/git-autod/src"
	. "gitlab.com/AOrps/git-autod/util"
)


func Run(ctx context.Context, cmd *cli.Command) error {
	directory := cmd.String("directory")
	fmt.Println(directory)

	
	err := src.GitStatus(directory)
	ErrorHandle(err)

	return nil
}
