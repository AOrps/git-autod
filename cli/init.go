package cli

import (
	"context"
	"fmt"
	"io"
	// "log"
	"net/http"
	"os"
	"time"
	"path/filepath"
	. "gitlab.com/AOrps/git-autod/util"

	"github.com/urfave/cli/v3"
)



// helper_CreateDirectory: helper function to create n directories. useful when n > 1.
func helper_CreateDirectory(path string) error {
	_, err := os.Stat(path)
	if err == nil {
		return nil
	}

	// If the directory does not exist, create its parent
	if os.IsNotExist(err) {
		err = helper_CreateDirectory(filepath.Dir(path))
		if err != nil {
			return err
		}
		// Makes the assumption that there exists protocols
		// in place for umask 
		err = os.Mkdir(path, 0777)
		if err != nil {
			return err
		}
	}
	return nil
}

// helper_DownloadLargeFile: do partial http range request to get the data in segmented bits
func helper_DownloadLargeFile(client *http.Client, url string, f *os.File) error {

	req, err := http.NewRequest("GET",url, nil)
	ErrorHandle(err)

	resp, err := client.Do(req)
	ErrorHandle(err)
	defer resp.Body.Close()

	contentLength := resp.ContentLength

	chunkSize := int64(1024 * 1024 * 100) // 100 MB

	var i int64
	for i = 0; i < contentLength; i += chunkSize {
		fmt.Printf("Download Percentage: %.5f %%\n", float64(i)/float64(contentLength)*float64(100))		
		iChunkSize := i + chunkSize - 1

		if iChunkSize > contentLength {
			iChunkSize = contentLength
		}

		bRange := fmt.Sprintf("bytes=%d-%d",i, iChunkSize)
		
		req.Header.Add("Range", bRange)
	
		resp, err := client.Do(req)
		ErrorHandle(err)

		b, err := io.ReadAll(resp.Body)
		ErrorHandle(err)
		f.Write(b)

		req.Header.Del("Range") // only have one range in the chamber
		// fmt.Printf("Download Percentage: %.5f %%\n", float64(iChunkSize)/float64(contentLength)*float64(100))		
	}
	fmt.Printf("Download Percentage: %.5f %%\n", float64(100))	
	


	return nil
}


// Init: `Init` command entrypoint
func Init(ctx context.Context, cmd *cli.Command) error {
	fmt.Println("Init")

	// TODO: change ctx with production urls and areas
	llmPath := ctx.Value("LOCAL_LLM_PATH").(string)
	testModelUrl := ctx.Value("TEST_MODEL_URL").(string)
	testModelName := ctx.Value("TEST_MODEL_NAME").(string)

	err := os.MkdirAll(llmPath, os.ModePerm)
	ErrorHandle(err)
	
	// At this point, a directory exists somewhere on the computer.
	// Ensure that a model gets into the machine. 
	
	tr := &http.Transport{
		MaxIdleConns: 10,
		IdleConnTimeout: 15 * time.Second,
		TLSHandshakeTimeout: 5 * time.Second,
	}
	
	client := &http.Client{
		Transport: tr,
	}
	
	modelPath := fmt.Sprintf("%s/%s", llmPath, testModelName)

	_, err = os.Stat(modelPath)

	// If caught here, do everything
	if os.IsNotExist(err) {
		ofile, err := os.Create(modelPath)
		ErrorHandle(err)

		// TODO: do dependency injection here
		err = helper_DownloadLargeFile(client, testModelUrl, ofile)
		ErrorHandle(err)

	}
			
	return nil
}


