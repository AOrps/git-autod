package src

import (
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"strings"

	"github.com/go-git/go-git/v5"
	. "gitlab.com/AOrps/git-autod/util"
	// "github.com/go-git/go-git/v5/plumbing"
	// "github.com/go-git/go-git/v5/storage/memory"
)

// TODO: Ensure diff works with files not added


func GitStatus(dir string) error {
	repo, err := git.PlainOpen(dir)
	ErrorHandle(err)

	w, err := repo.Worktree()
	ErrorHandle(err)

	status, err := w.Status()
	ErrorHandle(err)

	fmt.Println("Repository:")
	fmt.Println(repo)

	ref, err := repo.Head()
	fmt.Println(ref)

	commitObj, err := repo.CommitObject(ref.Hash())
	ErrorHandle(err)

	
	fmt.Println("Status:")
	fmt.Println(status)

	
	
	// for i,v := range(status) {

	// 	// coFile, err := commitObj.File(i)
	// }
	
	

	for i,v := range(status) {
		fmt.Println("i:", i, reflect.TypeOf(i))
		fmt.Println("v:", v, reflect.TypeOf(v))   // \v: &{32 77 } *git.FileStatus
		// fmt.Println(v.Extra, v.Staging, v.Worktree) // \ 32 77
		// fmt.Println("status.File(i):", status.File(i))
		// fmt.Println("status[i].Extra:", status[i].Extra)
		// fmt.Println(v.Staging)

		
		// This v.Staging != v.Worktree, is important
		// Prevents files that aren't in Staging (not added) from mucking things up
		// To handle this properly (in the future), add file and make llm generate content from the local file. Don't both with diff

		if v.Staging != v.Worktree {

			coFile, err := commitObj.File(i)
			ErrorHandle(err)
			// if err != nil {
			// 	fmt.Println("err: file not found", err)
			// 	if os.IsNotExist(err) {
			// 		fmt.Println(i, "is a new file")
			// 		continue
			// 	}
			// 	fmt.Print("ysad")
			// }
			// fmt.Println("coFile:")
			// fmt.Println(coFile.Lines())
			lines, err := coFile.Lines()

			// Old Line
			for j,line := range(lines) {
				// Old Line
				fmt.Println(j, line)
			}


			file, err := os.Open(i)
			ErrorHandle(err)

			defer file.Close()


			data, err := ioutil.ReadAll(file)
			ErrorHandle(err)
			// fmt.Println(string(data))

			newLines := strings.Split(string(data), "\n")

			
			
			// New Line
			for k,line := range(newLines) {
				fmt.Println(k,line)
			}


			diffs, err := DiffSlice(lines, newLines)
			ErrorHandle(err)

			fmt.Println("Differences between slices:")
			for _,diff := range diffs {
				switch diff.Type {
				case "add":
					fmt.Printf("+ %s\n", diff.Element)
				case "delete":
					fmt.Printf("- %s\n", diff.Element)
				case "equal":
					fmt.Printf(" %s\n", diff.Element)

				}
			}
		}
		// for i,v := coFile.Lines {
		// 	fmt.Println(i)
		// 	fmt.Println(v)
		// }
	}
	
	// fmt.Println(status.String())

	return nil
}
