# MVP (Diff)

## Intended Functionality

### To Run
```sh
git diff
```


### Output
```txt
diff --git a/cli/run.go b/cli/run.go
index 345ce30..67b1f55 100644
--- a/cli/run.go
+++ b/cli/run.go
@@ -2,7 +2,7 @@ package cli
 
 import (
 	"context"
-	// "fmt"
+	"fmt"
 
 	"github.com/urfave/cli/v3"
 	"gitlab.com/AOrps/git-autod/src"
@@ -11,7 +11,10 @@ import (
 
 
 func Run(ctx context.Context, cmd *cli.Command) error {
-	directory := cmd.String("directory")	
+	directory := cmd.String("directory")
+	fmt.Println(directory)
+
+	
 	err := src.GitStatus(directory)
 	ErrorHandle(err)
 
diff --git a/src/app.go b/src/app.go
index a564080..1a02d78 100644
--- a/src/app.go
+++ b/src/app.go
@@ -2,12 +2,15 @@ package src
 
 import (
 	"fmt"
+	"io/ioutil"
+	"os"
+	"reflect"
+	"strings"
 
-	. "gitlab.com/AOrps/git-autod/util"
 	"github.com/go-git/go-git/v5"
+	. "gitlab.com/AOrps/git-autod/util"
 	// "github.com/go-git/go-git/v5/plumbing"
 	// "github.com/go-git/go-git/v5/storage/memory"
-	
 )
 
 func GitStatus(dir string) error {
@@ -20,12 +23,82 @@ func GitStatus(dir string) error {
 	status, err := w.Status()
 	ErrorHandle(err)
 
-	fmt.Println(status)
-
+	fmt.Println("Repository:")
 	fmt.Println(repo)
 
 	ref, err := repo.Head()
 	fmt.Println(ref)
 
+	commitObj, err := repo.CommitObject(ref.Hash())
+	ErrorHandle(err)
+
+	
+	fmt.Println("Status:")
+	fmt.Println(status)
+
+	for i,_ := range(status) {
+		fmt.Println("i:", i, reflect.TypeOf(i))
+		// fmt.Println("v:", v, reflect.TypeOf(v))   # \v: &{32 77 } *git.FileStatus
+		// fmt.Println(v.Extra, v.Staging, v.Worktree) # \ 32 77
+		// fmt.Println("status.File(i):", status.File(i))
+		// fmt.Println("status[i].Extra:", status[i].Extra)
+
+		coFile, err := commitObj.File(i)
+		ErrorHandle(err)
+		// fmt.Println("coFile:")
+		// fmt.Println(coFile.Lines())
+		lines, err := coFile.Lines()
+
+		// Old Line
+		for j,line := range(lines) {
+			// Old Line
+			fmt.Println(j, line)
+		}
+
+
+		file, err := os.Open(i)
+		ErrorHandle(err)
+
+		defer file.Close()
+
+
+		data, err := ioutil.ReadAll(file)
+		ErrorHandle(err)
+		// fmt.Println(string(data))
+
+		newLines := strings.Split(string(data), "\n")
+
+		
+		
+		// New Line
+		for k,line := range(newLines) {
+			fmt.Println(k,line)
+		}
+
+
+		diffs, err := DiffSlice(lines, newLines)
+		ErrorHandle(err)
+
+		fmt.Println("Differences between slices:")
+		for _,diff := range diffs {
+			switch diff.Type {
+			case "add":
+				fmt.Printf("+ %s\n", diff.Element)
+			case "delete":
+				fmt.Printf("- %s\n", diff.Element)
+			case "equal":
+				fmt.Printf(" %s\n", diff.Element)
+
+			}
+		}
+		
+		// for i,v := coFile.Lines {
+		// 	fmt.Println(i)
+		// 	fmt.Println(v)
+		// }
+	}
+	
+	// fmt.Println(status.String())
+
 	return nil
 }
diff --git a/util/main.go b/util/main.go
index 833cabf..5205c95 100644
--- a/util/main.go
+++ b/util/main.go
@@ -1,12 +1,76 @@
 package util
 
 import (
+	"fmt"
 	"log"
 )
 
+type SliceDiff struct {
+	Type    string // Type can be "add", "delete", or "equal".
+	Element string // The element value in the slice.
+	Hash    string // Use a hash to de-dup the an entry
+}
+
+
 // ErrorHandle: handles errors thrown
 func ErrorHandle(e error) {
 	if e != nil {
 		log.Fatal(e)
 	}
 }
+
+
+// DiffSlice: Does a diff based around 2 []string's
+func DiffSlice(sA, sB []string) ([]SliceDiff, error) {
+	// 
+
+	if len(sA) == 0 && len(sB) == 0 {
+		return nil, fmt.Errorf("both slices are empty")
+	}
+
+	diff := make([]SliceDiff, 0)
+	// mapCheck := map --> 
+
+	i := 0
+	j := 0
+
+	for i < len(sA) || j < len(sB) {
+		if i == len(sA) {
+			diff = append(diff, SliceDiff{Type: "add", Element: sB[j]})
+			j++
+		} else if j == len(sB) {
+			diff = append(diff, SliceDiff{Type: "delete", Element: sA[i]})
+			i++
+		} else if sA[i] < sB[j] {
+			diff = append(diff, SliceDiff{Type: "delete", Element: sA[i]})
+			i++
+		} else if sA[i] > sB[j] {
+			diff = append(diff, SliceDiff{Type: "add", Element: sB[j]})
+			j++
+		} else {
+			diff = append(diff, SliceDiff{Type: "equal", Element: sA[i]})
+			i++
+			j++
+		}
+	}
+
+	if len(sA) != 0 && len(sB) != 0 {
+		last := sA[len(sA)-1]
+
+		for j < len(sB) && sB[j] > last {
+			diff = append(diff, SliceDiff{Type: "add", Element: sB[j]})
+			j++
+		}
+	}
+
+	if len(sA) != 0 && len(sB) != 0 {
+		last := sB[len(sB)-1]
+		for i < len(sA) && sA[i] > last {
+			diff = append(diff, SliceDiff{Type: "delete", Element: sA[i]})
+			i++
+		}
+	}
+
+	return diff, nil
+	
+}

```



## Current Functionality

### To Run
```sh
go build && ./git-autod run --directory=$(pwd)
```

### Output
```txt
/home/aorps/projects/git-autod
/home/aorps/projects/git-autod
Repository:
&{0xc00013c370 map[] 0xc00015dc60}
d5abd891b24f55aaaaddd8fca9d50687befe2db6 refs/heads/main
Status:
 M cli/run.go
 M src/app.go
?? src/goal.md
?? try.txt
 M util/main.go

i: cli/run.go string
v: &{32 77 } *git.FileStatus
0 package cli
1 
2 import (
3 	"context"
4 	// "fmt"
5 
6 	"github.com/urfave/cli/v3"
7 	"gitlab.com/AOrps/git-autod/src"
8 	. "gitlab.com/AOrps/git-autod/util"
9 )
10 
11 
12 func Run(ctx context.Context, cmd *cli.Command) error {
13 	directory := cmd.String("directory")	
14 	err := src.GitStatus(directory)
15 	ErrorHandle(err)
16 
17 	return nil
18 }
0 package cli
1 
2 import (
3 	"context"
4 	"fmt"
5 
6 	"github.com/urfave/cli/v3"
7 	"gitlab.com/AOrps/git-autod/src"
8 	. "gitlab.com/AOrps/git-autod/util"
9 )
10 
11 
12 func Run(ctx context.Context, cmd *cli.Command) error {
13 	directory := cmd.String("directory")
14 	fmt.Println(directory)
15 
16 	
17 	err := src.GitStatus(directory)
18 	ErrorHandle(err)
19 
20 	return nil
21 }
22 
Differences between slices:
 package cli
 
 import (
 	"context"
+ 	"fmt"
+ 
+ 	"github.com/urfave/cli/v3"
+ 	"gitlab.com/AOrps/git-autod/src"
+ 	. "gitlab.com/AOrps/git-autod/util"
- 	// "fmt"
- 
- 	"github.com/urfave/cli/v3"
- 	"gitlab.com/AOrps/git-autod/src"
- 	. "gitlab.com/AOrps/git-autod/util"
 )
 
 
 func Run(ctx context.Context, cmd *cli.Command) error {
+ 	directory := cmd.String("directory")
- 	directory := cmd.String("directory")	
- 	err := src.GitStatus(directory)
- 	ErrorHandle(err)
- 
+ 	fmt.Println(directory)
+ 
+ 	
+ 	err := src.GitStatus(directory)
+ 	ErrorHandle(err)
+ 
 	return nil
 }
+ 
i: src/app.go string
v: &{32 77 } *git.FileStatus
0 package src
1 
2 import (
3 	"fmt"
4 
5 	. "gitlab.com/AOrps/git-autod/util"
6 	"github.com/go-git/go-git/v5"
7 	// "github.com/go-git/go-git/v5/plumbing"
8 	// "github.com/go-git/go-git/v5/storage/memory"
9 	
10 )
11 
12 func GitStatus(dir string) error {
13 	repo, err := git.PlainOpen(dir)
14 	ErrorHandle(err)
15 
16 	w, err := repo.Worktree()
17 	ErrorHandle(err)
18 
19 	status, err := w.Status()
20 	ErrorHandle(err)
21 
22 	fmt.Println(status)
23 
24 	fmt.Println(repo)
25 
26 	ref, err := repo.Head()
27 	fmt.Println(ref)
28 
29 	return nil
30 }
0 package src
1 
2 import (
3 	"fmt"
4 	"io/ioutil"
5 	"os"
6 	"reflect"
7 	"strings"
8 
9 	"github.com/go-git/go-git/v5"
10 	. "gitlab.com/AOrps/git-autod/util"
11 	// "github.com/go-git/go-git/v5/plumbing"
12 	// "github.com/go-git/go-git/v5/storage/memory"
13 )
14 
15 // TODO: Ensure diff works with files not added
16 
17 
18 func GitStatus(dir string) error {
19 	repo, err := git.PlainOpen(dir)
20 	ErrorHandle(err)
21 
22 	w, err := repo.Worktree()
23 	ErrorHandle(err)
24 
25 	status, err := w.Status()
26 	ErrorHandle(err)
27 
28 	fmt.Println("Repository:")
29 	fmt.Println(repo)
30 
31 	ref, err := repo.Head()
32 	fmt.Println(ref)
33 
34 	commitObj, err := repo.CommitObject(ref.Hash())
35 	ErrorHandle(err)
36 
37 	
38 	fmt.Println("Status:")
39 	fmt.Println(status)
40 
41 	
42 	
43 	// for i,v := range(status) {
44 
45 	// 	// coFile, err := commitObj.File(i)
46 	// }
47 	
48 	
49 
50 	for i,v := range(status) {
51 		fmt.Println("i:", i, reflect.TypeOf(i))
52 		fmt.Println("v:", v, reflect.TypeOf(v))   // \v: &{32 77 } *git.FileStatus
53 		// fmt.Println(v.Extra, v.Staging, v.Worktree) // \ 32 77
54 		// fmt.Println("status.File(i):", status.File(i))
55 		// fmt.Println("status[i].Extra:", status[i].Extra)
56 		// fmt.Println(v.Staging)
57 
58 		
59 		// This v.Staging != v.Worktree, is important
60 		// Prevents files that aren't in Staging (not added) from mucking things up
61 		if v.Staging != v.Worktree {
62 
63 			coFile, err := commitObj.File(i)
64 			ErrorHandle(err)
65 			// if err != nil {
66 			// 	fmt.Println("err: file not found", err)
67 			// 	if os.IsNotExist(err) {
68 			// 		fmt.Println(i, "is a new file")
69 			// 		continue
70 			// 	}
71 			// 	fmt.Print("ysad")
72 			// }
73 			// fmt.Println("coFile:")
74 			// fmt.Println(coFile.Lines())
75 			lines, err := coFile.Lines()
76 
77 			// Old Line
78 			for j,line := range(lines) {
79 				// Old Line
80 				fmt.Println(j, line)
81 			}
82 
83 
84 			file, err := os.Open(i)
85 			ErrorHandle(err)
86 
87 			defer file.Close()
88 
89 
90 			data, err := ioutil.ReadAll(file)
91 			ErrorHandle(err)
92 			// fmt.Println(string(data))
93 
94 			newLines := strings.Split(string(data), "\n")
95 
96 			
97 			
98 			// New Line
99 			for k,line := range(newLines) {
100 				fmt.Println(k,line)
101 			}
102 
103 
104 			diffs, err := DiffSlice(lines, newLines)
105 			ErrorHandle(err)
106 
107 			fmt.Println("Differences between slices:")
108 			for _,diff := range diffs {
109 				switch diff.Type {
110 				case "add":
111 					fmt.Printf("+ %s\n", diff.Element)
112 				case "delete":
113 					fmt.Printf("- %s\n", diff.Element)
114 				case "equal":
115 					fmt.Printf(" %s\n", diff.Element)
116 
117 				}
118 			}
119 		}
120 		// for i,v := coFile.Lines {
121 		// 	fmt.Println(i)
122 		// 	fmt.Println(v)
123 		// }
124 	}
125 	
126 	// fmt.Println(status.String())
127 
128 	return nil
129 }
130 
Differences between slices:
 package src
 
 import (
 	"fmt"
- 
+ 	"io/ioutil"
+ 	"os"
+ 	"reflect"
+ 	"strings"
+ 
+ 	"github.com/go-git/go-git/v5"
 	. "gitlab.com/AOrps/git-autod/util"
- 	"github.com/go-git/go-git/v5"
 	// "github.com/go-git/go-git/v5/plumbing"
 	// "github.com/go-git/go-git/v5/storage/memory"
- 	
 )
 
+ // TODO: Ensure diff works with files not added
+ 
+ 
 func GitStatus(dir string) error {
 	repo, err := git.PlainOpen(dir)
 	ErrorHandle(err)
 
 	w, err := repo.Worktree()
 	ErrorHandle(err)
 
 	status, err := w.Status()
 	ErrorHandle(err)
 
+ 	fmt.Println("Repository:")
+ 	fmt.Println(repo)
+ 
- 	fmt.Println(status)
- 
- 	fmt.Println(repo)
- 
 	ref, err := repo.Head()
 	fmt.Println(ref)
 
+ 	commitObj, err := repo.CommitObject(ref.Hash())
+ 	ErrorHandle(err)
+ 
+ 	
+ 	fmt.Println("Status:")
+ 	fmt.Println(status)
+ 
+ 	
+ 	
+ 	// for i,v := range(status) {
+ 
+ 	// 	// coFile, err := commitObj.File(i)
+ 	// }
+ 	
+ 	
+ 
+ 	for i,v := range(status) {
+ 		fmt.Println("i:", i, reflect.TypeOf(i))
+ 		fmt.Println("v:", v, reflect.TypeOf(v))   // \v: &{32 77 } *git.FileStatus
+ 		// fmt.Println(v.Extra, v.Staging, v.Worktree) // \ 32 77
+ 		// fmt.Println("status.File(i):", status.File(i))
+ 		// fmt.Println("status[i].Extra:", status[i].Extra)
+ 		// fmt.Println(v.Staging)
+ 
+ 		
+ 		// This v.Staging != v.Worktree, is important
+ 		// Prevents files that aren't in Staging (not added) from mucking things up
+ 		if v.Staging != v.Worktree {
+ 
+ 			coFile, err := commitObj.File(i)
+ 			ErrorHandle(err)
+ 			// if err != nil {
+ 			// 	fmt.Println("err: file not found", err)
+ 			// 	if os.IsNotExist(err) {
+ 			// 		fmt.Println(i, "is a new file")
+ 			// 		continue
+ 			// 	}
+ 			// 	fmt.Print("ysad")
+ 			// }
+ 			// fmt.Println("coFile:")
+ 			// fmt.Println(coFile.Lines())
+ 			lines, err := coFile.Lines()
+ 
+ 			// Old Line
+ 			for j,line := range(lines) {
+ 				// Old Line
+ 				fmt.Println(j, line)
+ 			}
+ 
+ 
+ 			file, err := os.Open(i)
+ 			ErrorHandle(err)
+ 
+ 			defer file.Close()
+ 
+ 
+ 			data, err := ioutil.ReadAll(file)
+ 			ErrorHandle(err)
+ 			// fmt.Println(string(data))
+ 
+ 			newLines := strings.Split(string(data), "\n")
+ 
+ 			
+ 			
+ 			// New Line
+ 			for k,line := range(newLines) {
+ 				fmt.Println(k,line)
+ 			}
+ 
+ 
+ 			diffs, err := DiffSlice(lines, newLines)
+ 			ErrorHandle(err)
+ 
+ 			fmt.Println("Differences between slices:")
+ 			for _,diff := range diffs {
+ 				switch diff.Type {
+ 				case "add":
+ 					fmt.Printf("+ %s\n", diff.Element)
+ 				case "delete":
+ 					fmt.Printf("- %s\n", diff.Element)
+ 				case "equal":
+ 					fmt.Printf(" %s\n", diff.Element)
+ 
+ 				}
+ 			}
+ 		}
+ 		// for i,v := coFile.Lines {
+ 		// 	fmt.Println(i)
+ 		// 	fmt.Println(v)
+ 		// }
- 	return nil
+ 	}
+ 	
+ 	// fmt.Println(status.String())
+ 
+ 	return nil
 }
+ 
i: src/goal.md string
v: &{63 63 } *git.FileStatus
i: try.txt string
v: &{63 63 } *git.FileStatus
i: util/main.go string
v: &{32 77 } *git.FileStatus
0 package util
1 
2 import (
3 	"log"
4 )
5 
6 // ErrorHandle: handles errors thrown
7 func ErrorHandle(e error) {
8 	if e != nil {
9 		log.Fatal(e)
10 	}
11 }
0 package util
1 
2 import (
3 	"fmt"
4 	"log"
5 )
6 
7 type SliceDiff struct {
8 	Type    string // Type can be "add", "delete", or "equal".
9 	Element string // The element value in the slice.
10 	Hash    string // Use a hash to de-dup the an entry
11 }
12 
13 
14 // ErrorHandle: handles errors thrown
15 func ErrorHandle(e error) {
16 	if e != nil {
17 		log.Fatal(e)
18 	}
19 }
20 
21 
22 // DiffSlice: Does a diff based around 2 []string's
23 func DiffSlice(sA, sB []string) ([]SliceDiff, error) {
24 	// 
25 
26 	if len(sA) == 0 && len(sB) == 0 {
27 		return nil, fmt.Errorf("both slices are empty")
28 	}
29 
30 	diff := make([]SliceDiff, 0)
31 	// mapCheck := map --> 
32 
33 	i := 0
34 	j := 0
35 
36 	for i < len(sA) || j < len(sB) {
37 		if i == len(sA) {
38 			diff = append(diff, SliceDiff{Type: "add", Element: sB[j]})
39 			j++
40 		} else if j == len(sB) {
41 			diff = append(diff, SliceDiff{Type: "delete", Element: sA[i]})
42 			i++
43 		} else if sA[i] < sB[j] {
44 			diff = append(diff, SliceDiff{Type: "delete", Element: sA[i]})
45 			i++
46 		} else if sA[i] > sB[j] {
47 			diff = append(diff, SliceDiff{Type: "add", Element: sB[j]})
48 			j++
49 		} else {
50 			diff = append(diff, SliceDiff{Type: "equal", Element: sA[i]})
51 			i++
52 			j++
53 		}
54 	}
55 
56 	if len(sA) != 0 && len(sB) != 0 {
57 		last := sA[len(sA)-1]
58 
59 		for j < len(sB) && sB[j] > last {
60 			diff = append(diff, SliceDiff{Type: "add", Element: sB[j]})
61 			j++
62 		}
63 	}
64 
65 	if len(sA) != 0 && len(sB) != 0 {
66 		last := sB[len(sB)-1]
67 		for i < len(sA) && sA[i] > last {
68 			diff = append(diff, SliceDiff{Type: "delete", Element: sA[i]})
69 			i++
70 		}
71 	}
72 
73 	return diff, nil
74 	
75 }
76 
Differences between slices:
 package util
 
 import (
+ 	"fmt"
 	"log"
 )
 
- // ErrorHandle: handles errors thrown
- func ErrorHandle(e error) {
- 	if e != nil {
- 		log.Fatal(e)
- 	}
+ type SliceDiff struct {
+ 	Type    string // Type can be "add", "delete", or "equal".
+ 	Element string // The element value in the slice.
+ 	Hash    string // Use a hash to de-dup the an entry
 }
+ 
+ 
+ // ErrorHandle: handles errors thrown
+ func ErrorHandle(e error) {
+ 	if e != nil {
+ 		log.Fatal(e)
+ 	}
+ }
+ 
+ 
+ // DiffSlice: Does a diff based around 2 []string's
+ func DiffSlice(sA, sB []string) ([]SliceDiff, error) {
+ 	// 
+ 
+ 	if len(sA) == 0 && len(sB) == 0 {
+ 		return nil, fmt.Errorf("both slices are empty")
+ 	}
+ 
+ 	diff := make([]SliceDiff, 0)
+ 	// mapCheck := map --> 
+ 
+ 	i := 0
+ 	j := 0
+ 
+ 	for i < len(sA) || j < len(sB) {
+ 		if i == len(sA) {
+ 			diff = append(diff, SliceDiff{Type: "add", Element: sB[j]})
+ 			j++
+ 		} else if j == len(sB) {
+ 			diff = append(diff, SliceDiff{Type: "delete", Element: sA[i]})
+ 			i++
+ 		} else if sA[i] < sB[j] {
+ 			diff = append(diff, SliceDiff{Type: "delete", Element: sA[i]})
+ 			i++
+ 		} else if sA[i] > sB[j] {
+ 			diff = append(diff, SliceDiff{Type: "add", Element: sB[j]})
+ 			j++
+ 		} else {
+ 			diff = append(diff, SliceDiff{Type: "equal", Element: sA[i]})
+ 			i++
+ 			j++
+ 		}
+ 	}
+ 
+ 	if len(sA) != 0 && len(sB) != 0 {
+ 		last := sA[len(sA)-1]
+ 
+ 		for j < len(sB) && sB[j] > last {
+ 			diff = append(diff, SliceDiff{Type: "add", Element: sB[j]})
+ 			j++
+ 		}
+ 	}
+ 
+ 	if len(sA) != 0 && len(sB) != 0 {
+ 		last := sB[len(sB)-1]
+ 		for i < len(sA) && sA[i] > last {
+ 			diff = append(diff, SliceDiff{Type: "delete", Element: sA[i]})
+ 			i++
+ 		}
+ 	}
+ 
+ 	return diff, nil
+ 	
+ }
+ 

```
