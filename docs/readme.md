# docs

## Directory Explanation
<!-- tree command: tree -d --noreport -->

```s
.
├── cli         ; logic for the cli commands
├── docs        ; any documentation exists here
├── model       ; (generated) local place where the models will exist
├── src         ; core logic (git and ollama magic happen here)
└── util        ; tools that can be used across the module
```


## External Documentation
### Cli (urfave/cli)
- https://github.com/urfave/cli/tree/main/docs/v3/examples

### Ollama API
- https://github.com/ollama/ollama/blob/main/docs/api.md#create-a-model
- https://github.com/ollama/ollama/blob/main/docs/modelfile.md

### go-git
- https://github.com/go-git/go-git/tree/master/_examples
- https://github.com/go-git/go-git/blob/master/COMPATIBILITY.md
- https://github.com/go-git/go-git/blob/master/plumbing/object/file.go#L76

### Art
```
                                                                          
                                         ::::::::                         
                                  :::::::  :::   ::                       
                             :::::::::::   ::::::::::::                   
                           ::    ::::::::::  :::::     ::                 
                          :  ::::          ::     :::: ::::               
                          :: :::  :::: ::    :     ::  ::  ::             
                            ::  ::          : ::       :: :::::           
                              ::::          :::    :::: ::  :: :::        
                         :::::  :::   :: :      :::    :   ::::   ::      
                       ::::::::::  ::::  :::  :::::::  :    ::     :      
                        :::::::::::  ::  ::::     :::: :     :::   :      
                  ::::    :::::::::::  :::     :::   : ::      ::::       
                :::::::::     :::::::::: :::  ::::  ::  :::       :       
             :::::::::::::      ::::::::::  ::    ::     ::  :: : :       
           :::::::::::::::       :::::::::::  ::  :      ::    :::        
         ::::::::::::::::::   ::   :::::::::::  :::  :: :      :          
       ::::::::::::::::::::   ::::     ::::::::::  :: ::      ::          
      :::::::::::::::::::::   :::::     ::::::::::   ::     ::            
       ::::::::::::::::::::   :::::     ::::::::       :::::              
         ::::::::::::::::::   ::::::::::::::::                            
            :::::::::::::::   ::::::::::::::                              
              ::::::::::::      :::::::::                                 
                ::::::::::     ::::::::                                   
                  :::::::::::::::::::                                     
                     ::::::::::::::                                       
                       ::::::::::                                         
                         :::::                                            
                                                                          
```
- https://www.asciiart.eu/image-to-ascii#google_vignette
- ![](./git-autod.png)
