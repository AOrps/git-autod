package util

import (
	"fmt"
	"log"
)

type SliceDiff struct {
	Type    string // Type can be "add", "delete", or "equal".
	Element string // The element value in the slice.
	Hash    string // Use a hash to de-dup the an entry
}


// ErrorHandle: handles errors thrown
func ErrorHandle(e error) {
	if e != nil {
		log.Fatal(e)
	}
}


// DiffSlice: Does a diff based around 2 []string's
func DiffSlice(sA, sB []string) ([]SliceDiff, error) {
	// 

	if len(sA) == 0 && len(sB) == 0 {
		return nil, fmt.Errorf("both slices are empty")
	}

	diff := make([]SliceDiff, 0)
	// mapCheck := map --> 

	i := 0
	j := 0

	for i < len(sA) || j < len(sB) {
		if i == len(sA) {
			diff = append(diff, SliceDiff{Type: "add", Element: sB[j]})
			j++
		} else if j == len(sB) {
			diff = append(diff, SliceDiff{Type: "delete", Element: sA[i]})
			i++
		} else if sA[i] < sB[j] {
			diff = append(diff, SliceDiff{Type: "delete", Element: sA[i]})
			i++
		} else if sA[i] > sB[j] {
			diff = append(diff, SliceDiff{Type: "add", Element: sB[j]})
			j++
		} else {
			diff = append(diff, SliceDiff{Type: "equal", Element: sA[i]})
			i++
			j++
		}
	}

	if len(sA) != 0 && len(sB) != 0 {
		last := sA[len(sA)-1]

		for j < len(sB) && sB[j] > last {
			diff = append(diff, SliceDiff{Type: "add", Element: sB[j]})
			j++
		}
	}

	if len(sA) != 0 && len(sB) != 0 {
		last := sB[len(sB)-1]
		for i < len(sA) && sA[i] > last {
			diff = append(diff, SliceDiff{Type: "delete", Element: sA[i]})
			i++
		}
	}

	return diff, nil
	
}
